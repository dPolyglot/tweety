from django.db import models


# Create your models here.
class TweetModel(models.Model):
    tweet = models.CharField(max_length=150, verbose_name="", unique=True)
    tweet_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.tweet
