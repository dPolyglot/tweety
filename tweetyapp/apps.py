from django.apps import AppConfig


class TweetyappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tweetyapp'
