from django.forms import ModelForm
from django import forms

from tweetyapp.models import TweetModel


class TweetForm(ModelForm):
    class Meta:
        model = TweetModel
        fields = "__all__"
        widgets = {
            "tweet": forms.Textarea(attrs={'placeholder': "What's on your mind?"})
        }
