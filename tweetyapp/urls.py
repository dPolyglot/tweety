from django.urls import path

from tweetyapp import views

urlpatterns = [
    path('', views.tweets, name="tweets")
]
