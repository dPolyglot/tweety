from django.shortcuts import render, redirect

# Create your views here.
from .forms import TweetForm
from .models import TweetModel


def tweets(request):
    if request.method == 'POST':
        tweet_form = TweetForm(request.POST)

        if tweet_form.is_valid():
            tweet_form.save()
            return redirect('tweets')

    else:
        tweet_form = TweetForm()

    all_tweets = TweetModel.objects.all()

    context = {
        'form': tweet_form, 'tweets': all_tweets
    }

    return render(request, 'tweets.html', context)
